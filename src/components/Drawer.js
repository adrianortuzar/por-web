import React from 'react';
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import {
    Link
} from 'react-router-dom'

export default class DrawerSimpleExample extends React.Component {

    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    render() {
        return (
            <div>
                <Drawer open={this.props.open}>
                    <MenuItem onClick={this.props.handleToggle}>
                        <Link to="/sensor" >Sensors</Link>
                    </MenuItem>
                    <MenuItem onClick={this.props.handleToggle}>
                        <Link to="/sensor/wind" >Wind Sensors</Link>
                    </MenuItem>
                    <MenuItem onClick={this.props.handleToggle}>
                        <Link to="/sensor/water-level">Water Level Sensors</Link>
                    </MenuItem>
                    <MenuItem onClick={this.props.handleToggle}>
                        <Link to="/sensor/wave">Waves Sensors</Link>
                    </MenuItem>
                    <MenuItem onClick={this.props.handleToggle}>
                        <Link to="/sensor/visibility">Visibility Sensors</Link>
                    </MenuItem>
                    <MenuItem onClick={this.props.handleToggle}>
                        <Link to="/sensor/salinity">Salinity Sensors</Link>
                    </MenuItem>
                    <MenuItem onClick={this.props.handleToggle}>
                        <Link to="/sensor/current">Currents Sensors</Link>
                    </MenuItem>
                    <MenuItem onClick={this.props.handleToggle}>
                        <Link to="/sensor/current">Currents Sensors</Link>
                    </MenuItem>
                </Drawer>
            </div>
        );
    }
}
