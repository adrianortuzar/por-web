import React from 'react';
import Api from '../../Services/Api'
import './StatusCircle.css'

export default class Sensors extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            status: 'Online'
        };

        var self = this;
        setInterval(function () {
            Api.getLastEvent(props.typeId, props.deviceId).then(function success(res) {

                self.setState({status:'Online'})
            }, function error() {

                self.setState({status:'Offline'})
            })
        }, 60000);

    }

    render() {
        return (
            <div className="por-statusCircle">
                <div className={'circle '+this.state.status}></div>
                <div className={'title '+this.state.status}>{this.state.status}</div>
            </div>
        )
    }
}
