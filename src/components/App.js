import React from 'react';
import AppBar from 'material-ui/AppBar';
import Drawer from './Drawer'
import Logged from './RightMenu'
import DevicesView from './deviceViews/DevicesView/DevicesView'
import { Route } from 'react-router-dom'
import DeviceDetailView from './deviceViews/DeviceDetailView/DeviceDetailView'

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};

    }

    handleToggle = () => this.setState({open: !this.state.open});


    render() {
        return (

            <div className="container-root">
                <Drawer open={this.state.open}
                        handleToggle={this.handleToggle}
                        className="container-root"
                />
                <AppBar
                    title={
                        <div style={{
                            display: 'flex'
                        }}>
                            <div className="por-title">
                                <span className="first">PoR</span> <span className="second">SMART INFRA</span>
                            </div>
                            <div className="por-appbar-button">
                                <div className="title">
                                    <div>Dashboard</div>
                                </div>
                                <div className="bar"></div>
                            </div>
                        </div>}
                    onLeftIconButtonTouchTap={this.handleToggle}
                    iconElementRight={<Logged />}
                    className='por-appbar'
                >

                </AppBar>

                <Route
                    exact
                    path="/app/device/:id"
                    component={DeviceDetailView} />

                <Route path="/app/devices" component={DevicesView} />

            </div>
        )
    }
}
