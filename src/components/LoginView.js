import React from 'react';
import {CardActions} from 'material-ui/Card';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import LoginAlert from './LoginAlert'
import DataManager from '../Services/DataManager'

const style = {
    width: '300px',
    margin: '0 auto'
}

const styleButton = {
    margin: 12,
};

const users = [
    {
        username:"admin",
        password:"admin1234"
    },
    {
        username:"viewer",
        password:"viewer1234"
    }
];

export default class LoginView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password:'',
            openAlert:false
        };

        this.handleTextFieldChange = this.handleTextFieldChange.bind(this);
        this.handleTextFieldChangePassword = this.handleTextFieldChangePassword.bind(this);
        this.loginAction = this.loginAction.bind(this);
    }

    handleTextFieldChange(e){
        this.setState({username:e.target.value});
    }

    handleTextFieldChangePassword(e){
        this.setState({password:e.target.value});
    }

    loginAction(){
        var self = this;
        var user = users.find(function (user) {
            return (self.state.password === user.password && self.state.username === user.username)
        })

        if (user){
            DataManager.currentUserName = user.username;
            this.props.history.push('/app/devices/list');
        }else{
            this.setState({openAlert:true});
        }
    }

    handleToggle = () => this.setState({openAlert: !this.state.openAlert});

    render(){
        return (
            <MuiThemeProvider>
                <div>
                    <LoginAlert open={this.state.openAlert}
                                action={this.handleToggle}/>
                    <AppBar
                        title="Port of Rotterdam"
                        showMenuIconButton={false}
                    />
                    <div style={style}>
                        <div>
                            <TextField
                                hintText=""
                                floatingLabelText="User name"
                                value={this.state.username}
                                onChange={this.handleTextFieldChange}
                            /><br />

                            <TextField
                                hintText=""
                                floatingLabelText="Password"
                                type="password"
                                value={this.state.password}
                                onChange={this.handleTextFieldChangePassword}
                            /><br />
                        </div>
                        <CardActions>
                            <RaisedButton
                                label="Login" primary={true} style={styleButton}
                                onTouchTap={this.loginAction}/>
                        </CardActions>
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}
