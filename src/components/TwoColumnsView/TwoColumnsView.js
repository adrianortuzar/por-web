import React from 'react';
import {Card} from 'material-ui/Card';
import './TwoColumnsView.css';

export default class TwoColumnsView extends React.Component {
    render() {
        return(
            <div className="TwoColumnsView">
                <div className="por-card">
                    <div className='title'>
                        {this.props.leftTitle}
                    </div>
                    <Card>
                        {this.props.leftComponent}
                    </Card>
                </div>

                <div className="por-card">
                    <div className='title'>
                        {this.props.rightTitle}
                    </div>
                    <Card>
                        {this.props.rightComponent}
                    </Card>
                </div>
            </div>
        )
    }
}
