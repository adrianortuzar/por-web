import React from 'react';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import DataManager from '../Services/DataManager'

export default class Logged extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password:'',
            openAlert:false
        };
        //this.handleTextFieldChange = this.handleTextFieldChange.bind(this);
        this.logoutAction = this.logoutAction.bind(this);
    }

    logoutAction(){
        DataManager.currentUserName = null;
        this._reactInternalInstance._context.router.history.push('/login');
    }

    render(){
        return(
            <IconMenu

                iconButtonElement={
                    <IconButton><MoreVertIcon /></IconButton>
                }
                targetOrigin={{horizontal: 'right', vertical: 'top'}}
                anchorOrigin={{horizontal: 'right', vertical: 'top'}}
            >
                <MenuItem primaryText="Settings" />
                <MenuItem primaryText="Profile" />
                <MenuItem primaryText="Sign out"
                          onTouchTap={this.logoutAction}
                />
            </IconMenu>
        )
    }
}
