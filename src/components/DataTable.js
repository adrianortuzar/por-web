import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

const DataTable = (props) => (
    <Table>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
                <TableHeaderColumn>Time</TableHeaderColumn>
                <TableHeaderColumn>Value</TableHeaderColumn>
            </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
            {props.data.map(datavar =>

                <TableRow
                    key={datavar.x}
                >
                    <TableRowColumn>
                      {datavar.x}
                    </TableRowColumn>
                    <TableRowColumn>
                      {datavar.y}
                    </TableRowColumn>
                </TableRow>
            )}
        </TableBody>
    </Table>
);
export default DataTable;
