import React from 'react';
import Paper from 'material-ui/Paper';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

const style = {
    margin: '0px 32px 0px 0px',
};

const MenuExampleSimple = () => (
    <div>
        <Paper style={style}>
            <Menu>
                <MenuItem primaryText="Refresh" />
                <MenuItem primaryText="Help &amp; feedback" />
                <MenuItem primaryText="Settings" />
                <MenuItem primaryText="Sign out" />
            </Menu>
        </Paper>
    </div>
);

export default MenuExampleSimple;