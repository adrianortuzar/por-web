import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */
export default class DialogExampleSimple extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open:this.props.open
        }
    }

    render() {
        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onTouchTap={this.props.action}
            />
        ];

        return (
            <div>
                <Dialog
                    title="Login Error"
                    actions={actions}
                    modal={false}
                    open={this.props.open}
                    onRequestClose={this.props.action}
                >
                    Username or password are incorrect. Please try again.
                </Dialog>
            </div>
        );
    }
}
