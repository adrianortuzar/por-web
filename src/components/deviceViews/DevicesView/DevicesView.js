import React from 'react';
import {CardText} from 'material-ui/Card';
import { Route } from 'react-router-dom';
import RaisedButton from 'material-ui/RaisedButton';
import DevicesListView from '../DevicesListView';
import TwoColumnsView from '../../TwoColumnsView/TwoColumnsView';
import DevicesMapView from '../DevicesMapView'
import Actionbutton from '../ActionButton/ActionButton';
import './DevicesView.css';

const style = {
    margin: 12,
};

export default class DevicesRouter extends React.Component {

    constructor(props) {
        super(props);

        this.gotoList = this.gotoList.bind(this);
        this.gotoMap = this.gotoMap.bind(this);
    }

    gotoMap(){
        this.props.history.push('/app/devices/map');
    }

    gotoList(){
        this.props.history.push('/app/devices/list');
    }

    render() {
        return(
            <TwoColumnsView
                leftTitle="DEVICES"
                leftComponent={
                    <div className="por-devicesView">
                        <div className="header">
                            <div className="title">
                                Connected devices: <span>84</span>
                            </div>
                            <div className="button-container">
                                <Actionbutton
                                    rightTitle="Map"
                                    actionRight={this.gotoMap}
                                    leftTitle="Table"
                                    actionLeft={this.gotoList}
                                />

                                <RaisedButton label="Add device" secondary={true} style={style} />
                            </div>
                        </div>
                        <div className="content">
                            <Route
                                exact
                                path="/app/devices/list"
                                component={DevicesListView}
                            />
                            <Route
                                exact
                                path="/app/devices/map"
                                component={DevicesMapView}
                            />
                        </div>
                    </div>
                }
                rightTitle="ALERTS"
                rightComponent={
                    <CardText>Alerts are not implemented yet</CardText>
                }
            />
        )
    }
}
