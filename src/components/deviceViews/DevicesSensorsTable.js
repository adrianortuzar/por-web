import React from 'react';
import {Table, TableBody, TableRow, TableRowColumn} from 'material-ui/Table';

var renderIcon = function (device) {
    if(device.gatewayId){
        return <i className="material-icons">power_input</i>
    }else{
        return <i className="material-icons">router</i>
    }
};

const DevicesTable = (props) => (
    <Table>
        <TableBody displayRowCheckbox={false}>
            {props.devices.map(device =>
                <TableRow
                    key={device.clientId}
                >
                    <TableRowColumn>
                        {renderIcon(device)}
                    </TableRowColumn>
                    <TableRowColumn>{device.deviceId}</TableRowColumn>
                    <TableRowColumn>{device.typeId}</TableRowColumn>
                </TableRow>
            )}

        </TableBody>
    </Table>
);

export default DevicesTable;
