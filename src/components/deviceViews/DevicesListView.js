import React from 'react';
import DataManager from '../../Services/DataManager'
import Api from '../../Services/Api'
import CircularProgress from 'material-ui/CircularProgress';
import DevicesTable from './DevicesTable';

const circularStyle = {
    position: 'initial',
    display: 'block',
    width: '40px',
    height: '40px',
    margin: '10px auto'
};

var getOrganizedDevices = function(devices){

    var gateaways = devices.filter(function (device) {
        return !device.gatewayId
    });

    var sensors = devices.filter(function (device) {
        return device.gatewayId
    });

    for (var i in sensors){
        var sensor = sensors[i];

        var gateaway = gateaways.find(function (gateaway) {
            return gateaway.deviceId === sensor.gatewayId
        })

        if (!gateaway.sensors){
            gateaway.sensors = [sensor];
        }
        else {
            gateaway.sensors.push(sensor);
        }
    }

    return devices;
}

export default class DevicesListView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            devices: [],
            currentUserName:DataManager.currentUserName,
            openDetail: false
        };


        this.goToDeviceDetailView = this.goToDeviceDetailView.bind(this);
    }

    componentDidMount() {
        var self = this;

        Api.getDevices.then(function success(res) {
            DataManager.devices = getOrganizedDevices(res.data.body.results);
            self.setState({"devices" : DataManager.devices});
        }, function error() {

        });
    }

    componentWillUnmount(){
        circularStyle.display = 'block';
    }

    selectedDevice = null;



    goToDeviceDetailView(clientId){
        this.props.history.push('/app/device/'+clientId);
    }



    render() {
        return (
            <div>
                <DevicesTable
                    devices={this.state.devices}
                    action={this.goToDeviceDetailView}
                    userRole={this.state.currentUserName}
                />
                <CircularProgress style={circularStyle}/>
            </div>

        );
    }
}
