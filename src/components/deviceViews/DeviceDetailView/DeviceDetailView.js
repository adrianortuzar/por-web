import React from 'react';
import {TableBody, TableRow, TableRowColumn} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress';
import IconButton from 'material-ui/IconButton';
import moment from 'moment';
import DevicesSensorsTable from '../DevicesSensorsTable';
import TwoColumnsView from '../../TwoColumnsView/TwoColumnsView'
import SimpleLineChart from '../../Charts';
import DataManager from '../../../Services/DataManager';
import ActionButton from '../ActionButton/ActionButton'
import DataTable from '../../DataTable';
import './DeviceDetailView.css'

const circularStyle = {
    position: 'initial',
    display: 'none',
    width: '40px',
    height: '40px',
    margin: '10px auto'
};

export default class DevicesListView extends React.Component {
    constructor(props) {
        super(props);
        this.close = this.close.bind(this);
        this.goBack = this.goBack.bind(this);
        this.showGraph = this.showGraph.bind(this);
        this.showTable = this.showTable.bind(this);

        var clientId = this.props.match.params.id
        var device = DataManager.getDeviceData(clientId);
        this.deviceid = device.deviceId;
        this.state = {
            title:"",
            loading:false,
            device:device,
            expanded: true,
            dataArray: []
        };

        this.addedSocket = false;
    };

    details = null

    componentWillReceiveProps(nextProps) {
        if(nextProps.open && nextProps.device && nextProps.device.deviceId){
            var self = this;
            this.setTitle((nextProps.device && nextProps.device.deviceId)?nextProps.device.deviceId:"");
            self.details = nextProps.device;
        }
    }

    goBack(){
      this.props.history.push('/app/devices/list');
    }

    showTable(){
      this.setState({expanded: false});
      console.log("show table");
    }

    showGraph(){
      this.setState({expanded: true});
      console.log("show graph");
    }

    close(){
        this.props.handleToggle();
    }

    setTitle(deviceId){
        this.setState({title:"Sensor "+deviceId})
    }

    render() {
      var self = this;
      if(!self.addedSocket){
        self.addedSocket = true;
        window.io.socket.on('deviceEvent', function (data) {
            if (data.deviceId === self.deviceid){
              if(undefined !== data.H){
                var value = parseInt(data.H.value);
                var dateStr = moment(data.phenomenonTime).valueOf();
                self.setState({dataArray: self.state.dataArray.concat({x: dateStr, y: value})});
                // self.state.dataArray.push([dateStr, value]);
              }
            }
        });
      }
        function SensorLocation(props) {
            if(props.gatewayId){
                return (
                    <div>

                    </div>
                )
            }else{
                return (<div></div>)
            }
        }
        function DeviceConnected(props) {
            if(props.gatewayId){
                return (
                    <div>
                        <h3><IconButton><i className="material-icons">router</i></IconButton>Gateway connected</h3>
                        <table>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow>
                                    <TableRowColumn>Gateway Id</TableRowColumn>
                                    <TableRowColumn>{props.gatewayId}</TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn>Gateway Type Id</TableRowColumn>
                                    <TableRowColumn>{props.gatewayTypeId}</TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </table>
                    </div>
                )
            }else{
                return (<div></div>);
            }
        }

        function LeftComponent(props) {
            return (
                <div>
                    <div className="topChart">
                      <div className="text">last recieved data: idk</div>
                      <ActionButton
                                rightTitle="Graph"
                                leftTitle="Table"
                              actionRight={self.showGraph}
                              actionLeft={self.showTable}
                              expanded={self.state.expanded}
                      />
                    </div>

                    <div hidden={!self.state.expanded}>
                        <SimpleLineChart
                            deviceId={props.deviceId}
                            type={props.type}
                            data={self.state.dataArray}
                        />
                    </div>

                    <div hidden={self.state.expanded}>
                        <DataTable
                            data={self.state.dataArray}
                        />
                    </div>
                </div>
            );
        }

        function RightComponent(props){
            return (
                <div>
                    <DeviceConnected
                        gatewayId={props.gatewayId}
                        gatewayTypeId={props.gatewayTypeId}
                    />
                    <div>
                        <h3><IconButton><i className="material-icons">date_range</i></IconButton>Registered</h3>
                        <table>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow>
                                    <TableRowColumn>Date</TableRowColumn>
                                    <TableRowColumn>{props.registrationDate}</TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn>Added by</TableRowColumn>
                                    <TableRowColumn>{props.registrationId}</TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </table>
                    </div>
                    <div hidden={props.gatewayId}>
                        <h3>Sensors connected</h3>
                        <DevicesSensorsTable
                            devices={props.sensors}
                        />
                    </div>
                    <div>
                        <h3><IconButton><i className="material-icons">memory</i></IconButton>Sensors details</h3>
                        <table>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow>
                                    <TableRowColumn>Type</TableRowColumn>
                                    <TableRowColumn>{props.type}</TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn>Device Id</TableRowColumn>
                                    <TableRowColumn>{props.deviceId}</TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn>Client Id</TableRowColumn>
                                    <TableRowColumn>{props.clientId}</TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </table>
                    </div>
                </div>
            )
        }

        function Details(props){
            if(props.loading){
                return (<div></div>);
            }else {
                return (
                    <div className="detail">
                      <div className="top">
                        <div className="title">
                          <div className="backButton" onTouchTap={self.goBack}><IconButton><i className="material-icons" >keyboard_backspace</i></IconButton><div>BACK TO DASHBOARD</div></div>
                          <div className="headTitle">{props.type}</div>
                          <div className="subTitle">{props.deviceId}</div>
                        </div>
                        <div className="map" >
                          <SensorLocation
                              gatewayId={props.gatewayId}
                          />
                        </div>
                      </div>
                        <TwoColumnsView
                            leftTitle="REALTIME DATA"
                            leftComponent={
                                <LeftComponent
                                    type={props.type}
                                    sensors={props.sensors}
                                    deviceId={props.deviceId}
                                    gatewayId={props.gatewayId}
                                />
                            }
                            rightTitle="SENSOR CONFIGURATION"
                            rightComponent={
                                <RightComponent
                                    gatewayId={props.gatewayId}
                                    gatewayTypeId={props.gatewayTypeId}
                                    registrationDate={props.registrationDate}
                                    registrationId={props.registrationId}
                                    type={props.type}
                                    sensors={props.sensors}
                                    deviceId={props.deviceId}
                                    clientId={props.clientId}
                                />
                            }
                        />
                    </div>
                )
            }
        };

        return (
            <div style={{
                height: '100%',
                display: 'flex',
                'flex-direction': 'column',
                'flex-grow': 1
            }}>
                <Details
                    loading={this.state.loading}
                    registrationDate={(this.state.device && this.state.device.registration.date)?this.state.device.registration.date:""}
                    registrationId={(this.state.device && this.state.device.registration.auth.id)?this.state.device.registration.auth.id:""}
                    type={(this.state.device && this.state.device.typeId)?this.state.device.typeId:""}
                    clientId={(this.state.device && this.state.device.clientId)?this.state.device.clientId:""}
                    deviceId={(this.state.device && this.state.device.deviceId)?this.state.device.deviceId:""}
                    gatewayId={(this.state.device && this.state.device.gatewayId)?this.state.device.gatewayId:""}
                    gatewayTypeId={(this.state.device && this.state.device.gatewayTypeId)?this.state.device.gatewayTypeId:""}
                    sensors={(this.state.device && this.state.device.sensors)?this.state.device.sensors:[]}
                    action={this.props.action}
                />
                <CircularProgress style={circularStyle}/>
            </div>
        );
    }
}
