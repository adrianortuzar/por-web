import React from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import {Table, TableBody, TableRow, TableRowColumn} from 'material-ui/Table';
import { withGoogleMap, GoogleMap } from "react-google-maps";
import RealDataSensorDialog from "../RealDataSensorDialog"
import DevicesSensorsTable from './DevicesSensorsTable'

/**
 * Dialog with action buttons. The actions are passed in as an array of React objects,
 * in this example [FlatButtons](/#/components/flat-button).
 *
 * You can also close this dialog by clicking outside the dialog, or with the 'Esc' key.
 */

const circularStyle = {
    position: 'initial',
    display: 'none',
    width: '40px',
    height: '40px',
    margin: '10px auto'
};

const GettingStartedGoogleMap = withGoogleMap(props => (
    <GoogleMap
        defaultZoom={10}
        defaultCenter={{ lat: 51.9317493, lng: 4.1015348 }}
    >
    </GoogleMap>
));

class LastEvent extends React.Component {


    render() {
        if(this.props.gatewayId){
            return (<h3>Last sensor data</h3>)
        }else{
            return (<div></div>)
        }
    }
}

export default class SensorDetail extends React.Component {

    constructor(props) {
        super(props);
        this.close = this.close.bind(this);
        this.state = {
            title:"",
            loading:false
        }
    };

    details = null

    componentWillReceiveProps(nextProps) {
        if(nextProps.open && nextProps.device && nextProps.device.deviceId){
            var self = this;
            this.setTitle((nextProps.device && nextProps.device.deviceId)?nextProps.device.deviceId:"");
            self.details = nextProps.device;
        }
    }

    close(){
        this.props.handleToggle();
    }

    setTitle(deviceId){
        this.setState({title:"Sensor "+deviceId})
    }

    render() {
        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                onTouchTap={this.close}
            />
        ];

        function SensorLocation(props) {
            if(props.gatewayId){
                return (
                    <div>
                        <h3>Sensor Location</h3>
                        <GettingStartedGoogleMap
                            containerElement={
                                <div style={{ height: `200px` }} />
                            }
                            mapElement={
                                <div style={{ height: `200px` }} />
                            }
                            gatewayId={props.gatewayId}
                        />
                    </div>
                )
            }else{
                return (<div></div>)
            }
        }
        function DeviceConnected(props) {
            if(props.gatewayId){
                return (
                    <div>
                        <h3>Gateway connected</h3>
                        <table>
                            <TableBody displayRowCheckbox={false}>
                                <TableRow>
                                    <TableRowColumn>Gateway Id</TableRowColumn>
                                    <TableRowColumn>{props.gatewayId}</TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn>Gateway Type Id</TableRowColumn>
                                    <TableRowColumn>{props.gatewayTypeId}</TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </table>
                    </div>
                )
            }else{
                return (<div></div>);
            }
        }


        function Details(props){
            if(props.loading){
                return (<div></div>);
            }else {
                return (
                    <div>
                        <h3></h3>
                        <div>
                            <RealDataSensorDialog
                                deviceId={props.deviceId}
                                type={props.type}
                            />
                        </div>
                        <div>
                            <h3>Sensor details</h3>
                            <Table>
                                <TableBody displayRowCheckbox={false}>
                                    <TableRow>
                                        <TableRowColumn>Type</TableRowColumn>
                                        <TableRowColumn>{props.type}</TableRowColumn>
                                    </TableRow>
                                    <TableRow>
                                        <TableRowColumn>Device Id</TableRowColumn>
                                        <TableRowColumn>{props.deviceId}</TableRowColumn>
                                    </TableRow>
                                    <TableRow>
                                        <TableRowColumn>Client Id</TableRowColumn>
                                        <TableRowColumn>{props.clientId}</TableRowColumn>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </div>
                        <DeviceConnected
                            gatewayId={props.gatewayId}
                            gatewayTypeId={props.gatewayTypeId}
                        />
                        <div>
                            <h3>Registered</h3>
                            <table>
                                <TableBody displayRowCheckbox={false}>
                                    <TableRow>
                                        <TableRowColumn>Date</TableRowColumn>
                                        <TableRowColumn>{props.registrationDate}</TableRowColumn>
                                    </TableRow>
                                    <TableRow>
                                        <TableRowColumn>Added by</TableRowColumn>
                                        <TableRowColumn>{props.registrationId}</TableRowColumn>
                                    </TableRow>
                                </TableBody>
                            </table>
                        </div>
                        <div hidden={props.gatewayId}>
                            <h3>Sensors connected</h3>
                            <DevicesSensorsTable
                                devices={props.sensors}
                            />
                        </div>
                        <LastEvent
                            gatewayId={props.gatewayId}
                        />
                        <SensorLocation
                            gatewayId={props.gatewayId}
                        />
                    </div>
                )
            }
        };

        return (
            <div>
                <Dialog
                    title={this.state.title}
                    actions={actions}
                    modal={false}
                    open={this.props.open}
                    onRequestClose={this.close}
                    autoScrollBodyContent={true}
                >
                    <Details
                        loading={this.state.loading}
                        registrationDate={(this.details && this.details.registration.date)?this.details.registration.date:""}
                        registrationId={(this.details && this.details.registration.auth.id)?this.details.registration.auth.id:""}
                        type={(this.details && this.details.typeId)?this.details.typeId:""}
                        clientId={(this.details && this.details.clientId)?this.details.clientId:""}
                        deviceId={(this.details && this.details.deviceId)?this.details.deviceId:""}
                        gatewayId={(this.details && this.details.gatewayId)?this.details.gatewayId:""}
                        gatewayTypeId={(this.details && this.details.gatewayTypeId)?this.details.gatewayTypeId:""}
                        sensors={(this.details && this.details.sensors)?this.details.sensors:[]}
                        action={this.props.action}
                    />
                    <CircularProgress style={circularStyle}/>
                </Dialog>
            </div>
        );
    }
}
