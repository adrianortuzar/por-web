import React from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import StatusCircle from '../StatusCircle/StatusCircle'

var renderIcon = function (device) {
    if(device.gatewayId){
        return <i className="material-icons">memory</i>
    }else{
        return <i className="material-icons">router</i>
    }
};

export default class DevicesTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    optionsAction(deviceId){
        debugger;
    }

    render(){
        return(
            <Table>
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                        <TableHeaderColumn>Device ID </TableHeaderColumn>
                        <TableHeaderColumn>Status</TableHeaderColumn>



                        <TableHeaderColumn>Device Type</TableHeaderColumn>
                        <TableHeaderColumn>Actions</TableHeaderColumn>
                        {/*<TableHeaderColumn hidden={DataManager.currentUserName === 'viewer'}>Reboot</TableHeaderColumn>*/}
                        {/*<TableHeaderColumn hidden={DataManager.currentUserName === 'viewer'}>Start/Stop</TableHeaderColumn>*/}
                        {/*<TableHeaderColumn hidden={DataManager.currentUserName === 'viewer'}>Delete</TableHeaderColumn>*/}
                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    {this.props.devices.map(device =>
                        <TableRow
                            key={device.clientId}
                            onTouchTap={() => this.props.action(device.clientId)}
                        >
                            <TableRowColumn>{renderIcon(device)} {device.deviceId}</TableRowColumn>
                            <TableRowColumn>
                                <StatusCircle
                                    typeId={device.typeId}
                                    deviceId={device.deviceId}
                                />
                            </TableRowColumn>

                            <TableRowColumn>{device.typeId}</TableRowColumn>
                            <TableRowColumn>
                                <FlatButton label="Options" primary={true} onTouchTap={this.optionsAction}/>
                            </TableRowColumn>
                            {/*<TableRowColumn hidden={DataManager.currentUserName === 'viewer'}>*/}
                            {/*<IconButton>*/}
                            {/*<i className="material-icons">autorenew</i>*/}
                            {/*</IconButton>*/}
                            {/*</TableRowColumn>*/}
                            {/*<TableRowColumn hidden={DataManager.currentUserName === 'viewer'}>*/}
                            {/*<IconButton>*/}
                            {/*<i className="material-icons">stop</i>*/}
                            {/*</IconButton>*/}
                            {/*</TableRowColumn>*/}
                            {/*<TableRowColumn hidden={DataManager.currentUserName === 'viewer'}>*/}
                            {/*<IconButton>*/}
                            {/*<i className="material-icons">backspace</i>*/}
                            {/*</IconButton>*/}
                            {/*</TableRowColumn>*/}
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        )
    }
}
