import React from 'react';
import { Map, Graphic, Symbols, Geometry } from 'react-arcgis';
import DataManager from '../../Services/DataManager'

export default class DevicesMapView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            devices: [],
            currentUserName:DataManager.currentUserName,
            value:"map"
        };
    }

    render() {
        return (
            <Map
                style={{ width: '100%', height: '500px' }}
                mapProperties={{
                    basemap: "topo-vector", //streets-vector / streets / hybrid / gray / topo / osm
                }}
                viewProperties={{
                    center: [4.305098, 51.895311],
                    zoom: 10
                }}
            >
                <Graphic>
                    <Symbols.SimpleMarkerSymbol
                        symbolProperties={{
                            color: [226, 119, 40],
                            outline: {
                                color: [255, 255, 255],
                                width: 2
                            }
                        }}
                    />
                    <Geometry.Point
                        geometryProperties={{
                            latitude: 51.895311,
                            longitude: 4.305098
                        }}
                    />
                </Graphic>
            </Map>
        );
    }
}
