import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import {CardActions} from 'material-ui/Card';
import './ActionButton.css'

export default class ActionButton extends React.Component {
    render(){
        return (
            <div className="actionButtons">
                <CardActions>
                    <FlatButton label={this.props.rightTitle} onTouchTap={this.props.actionRight} className={this.props.expanded === false ? 'inactive' : ''}/>
                    <FlatButton label={this.props.leftTitle} onTouchTap={this.props.actionLeft} className={this.props.expanded === true ? 'inactive' : ''}/>
                </CardActions>
            </div>
        )
    }
}
