var socketIOClient = require('socket.io-client');
var sailsIOClient = require('sails.io.js');
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Config from './Config'

import {
    BrowserRouter as Router,
    Route,
    Switch
} from 'react-router-dom'
import Login from './components/LoginView';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import App from './components/App'
import createBrowserHistory from 'history/createBrowserHistory'
const customHistory = createBrowserHistory()

// Instantiate the socket client (`io`)
// (for now, you must explicitly pass in the socket.io client when using this library from Node.js)
window.io = sailsIOClient(socketIOClient);
window.io.sails.url = Config.apiDomain;


// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();


export default class RouterApp extends React.Component {
    render() {
        return (
            <MuiThemeProvider>
                <Router history={customHistory}>
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <Route path="/login" component={Login} />
                        <Route path="/app" component={App} />
                    </Switch>
                </Router>
            </MuiThemeProvider>
        )
    }
}

ReactDOM.render(
    <RouterApp />,
    document.getElementById('root')
);
