//production domain
// https://hbr-poc-backendserver.eu-gb.mybluemix.net

// dev
//http://localhost:1337

let Config = {
    apiDomain:'https://hbr-poc-backendserver.eu-gb.mybluemix.net',
    apiUrl: function(){return this.apiDomain+'/api/v1'}
};


export default Config;
