let DataManager = {
    currentUserName:null,
    devices:[],
    getDeviceData: function(clientId) {
        if(this.devices.length){
            return this.devices.find(device => device.clientId === clientId);
        }
    }
};


export default DataManager;
