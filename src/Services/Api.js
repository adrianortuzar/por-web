import axios from 'axios'
import config from '../Config';

let Api = {
    getDevices: new Promise(function(resolve, reject) {
        // do a thing, possibly async, then…
        axios.get(config.apiUrl()+'/bulk/devices').then(function (res) {
            if (res.data) {
                resolve(res);
            }
            else {
                reject(Error(res));
            }
        });
    }),

    getLastEvent: function (typeId, deviceId) {
        return new Promise(function (resolve, reject) {
            ///api/v1
            axios.get(config.apiUrl()+'/device/types/'+typeId+'/devices/'+deviceId+'/data/live').then(function (res) {
                if (res.data && res.data.status === '200') {

                    resolve(res);
                }
                else {
                    reject(Error(res));
                }
            });
        })
    }
};


export default Api;
